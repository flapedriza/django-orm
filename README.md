- [Overview](#overview)
    - [Previous notes](#previous-notes)
    - [What is the Django ORM?](#what-is-the-django-orm)
- [The Django ORM](#the-django-orm)
    - [Models](#models)
    - [Relations](#relations)
    - [`QuerySet` objects](#queryset-objects)
- [Making Queries](#making-queries)
    - [Query methods](#query-methods)
        - [`all()`](#all)
        - [`get()`](#get)
        - [`create()`](#create)
        - [`delete()`](#delete)
        - [`filter()`](#filter)
        - [`exclude()`](#exclude)
        - [`order_by()`](#orderby)
        - [`distinct()`](#distinct)
        - [`values_list()`](#valueslist)
        - [`first()`](#first)
        - [`last()`](#last)
        - [`latest()`](#latest)
        - [`earliest()`](#earliest)
        - [`count()`](#count)
        - [`exists()`](#exists)
    - [Field lookups](#field-lookups)
        - [exact](#exact)
        - [iexact](#iexact)
        - [contains](#contains)
        - [icontains](#icontains)
        - [in](#in)
        - [gt](#gt)
        - [gte](#gte)
        - [lt](#lt)
        - [lte](#lte)
        - [startswith](#startswith)
        - [istartswith](#istartswith)
        - [endswith](#endswith)
        - [iendswith](#iendswith)
        - [range](#range)
        - [isnull](#isnull)
        - [regex](#regex)
    - [Complex lookups with `Q` objects](#complex-lookups-with-q-objects)
- [Aggregation](#aggregation)
    - [Aggregation functions](#aggregation-functions)
        - [Count](#count)
        - [Max](#max)
        - [Min](#min)
        - [StdDev](#stddev)
        - [Sum](#sum)
        - [Variance](#variance)

# Overview

## Previous notes
:warning: This is intended as reference documentation, not as the workshop's script

## What is the Django ORM?
Essentially, the Django ORM helps developers by adding an abstraction layer on top of the Database that allows data model creation and information retrieval and modification without concerning about SQL

Example:
(Note that after making the SQL query we will obtain raw data that we still need to "store" in some object)
Django ORM:
```python
UserSubscription.objects.filter(user__birth_year__gte=1999, user__email__icontains='immfly')
```
SQL Query:
```sql
SELECT * FROM "users_usersubscription" INNER JOIN "users_userprofile" ON ("users_usersubscription"."user_id" = "users_userprofile"."uuid") WHERE ("users_userprofile"."birth_year" >= 1999 AND UPPER("users_userprofile"."email"::text) LIKE UPPER(%immfly%)) ORDER BY "users_usersubscription"."created" DESC
```

# The Django ORM

## Models
>:information_source: **Objects and classes**: A _class_ is a "template" that defines **which** attributes and methods every instance (object) of it will have, and objects are instances of that class, meaning that they all have all the attributes and methods defined by the class but with their own values.

The base of the Django ORM are the _Models_, which are python classes and define the attributes (`Fileds`) and relations between the database objects.
To start working with a model you should at least know all its fields and relations.

An example of two related models:
```python
class Company(models.Model):
    """
    Model that represents a company with a name, address and document number
    """
    name = models.CharField('Company name', max_length=50)
    address = models.CharField('Address', max_length=150)
    document_number = models.CharField('NIF', max_length=10, unique=True)
    
class Department(models.Model):
    """
    Model that represents a department of a company
    """
    name = models.CharField('Name', max_length=50, null=False)
    company = models.ForeignKey(Company, related_name='departments', on_delete=models.SET_NULL,
                                null=True)
```
This translates to the following SQL code:
```sql
-- Create model Company
CREATE TABLE "companies_company" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(50) NOT NULL, "address" varchar(150) NOT NULL, "document_number" varchar(10) NOT NULL UNIQUE);
-- Create model Department
CREATE TABLE "companies_department" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(50) NOT NULL, "company_id" integer NULL REFERENCES "companies_company" ("id"));
```
## Relations
>:information_source: The Django ORM handles single object relations returning the object itself and multiple object ones returning a `QuerySet`, which will be discussed later

Since the Django ORM sits on top of a relational database, relations are a key aspect.
Relations between models are defined using special `Field` attributes.
* **`ForeignKey`**: This field represents a many-to-one relation (multiple model instances can be related to a single other model intance) and lives in the "many" object since this field only allows a relation to **one** particular object, but the "other side" objects can have multiple instances of the previous "pointing" to them. Those relations can be followed both ways using the field name from one object and the name defined in the `related_name` parameter from the other, which returns a `QuerySet`
Example: `UserSubscriptions` have a foreign key to the `UserProfile` model, that way a user can have multiple `Usersubscription`s but a `UserSubscription` only has one user assigned to it
* **`ManyToManyField`**: As its name indicates, this field defines a many-to-many relation between two objects, as with the previous one, the relation can be followed both ways and both will return a `QuerySet`
Example: `TravelGuide` objects have a many to many relation to the `TravelGuideSponsor` model, that way a guide can have multiple sponsors and a soponsor can sponsorize multiple guides.
* **`OneToOneField`**: This field defines a one-to-one relation between two objects and it can live in either one. It's the same as a `ForeignKey`but will enforce uniqueness in the relation (No object of this class can be related to the same as another), as with the other two, the relation can be followed both ways and will return an object in both cases.

## `QuerySet` objects

`QuerySet` objects are fundamental in the django ORM and we could see them as "filters" that when "executed" will retrieve all desired items from the database (although they are more than that). We can chain as many filters as we want on a `QuerySet` as long as they return a new `QuerySet`, ie: we can stack for example as many `filter` or `exclude` filters as we want, but since the `get` method returns an object we can not stack any more finters once that method is called (when explaining methods it will be stated wether they return or not a new `QuerySet`).
>:information_source:`<ModelName>.objects` returns a `QuerySet` containing all instances of `ModelName`.

Example: Imagine we have a `QuerySet` named  `q` that "contains" all `UserProfile` objects returned by `UserProfile.objects.all()`, and we want to filter them by created and exclude the immfly ones, we would use the following query:
```python
q.exclude(email__icontains='immfly').filter(created__gte='2018-01-18')
```
Since the `filter` method returns a queryset we could keep stacking filters on this queryset.
 
Knowing that, it makes sense that when we follow relations, if the relation involves multiple objects on the other side a `QuerySet` will be returned to be able to filter the related objects even further.

>:information_source: `QuerySet` objects are evaluated in a lazy way, this means that until we "use" the data for example by iterating over it or printing it, no DB operations will be performed

# Making Queries

## Query methods

To make queries to the database, we will mainly use those methods:

### `all()`
Returns all objects of the queryset

### `get()`
Returns the object matching the given lookup parameters, which should be in the format described in [Field lookups](#field-lookups).

`get()` raises `MultipleObjectsReturned` if more than one object was found.

`get()` raises a `DoesNotExist` exception if an object wasn’t found for the given parameters.

If you expect a queryset to return one row, you can use get() without any arguments to return the object for that row:
```python
entry = Entry.objects.filter(...).exclude(...).get()
```

### `create()`

A convenience method for creating an object and saving it all in one step:
```python
p = Person.objects.create(first_name="Tipo", last_name="De Incógnito")
```
Returns the newly created object.

### `delete()`
Performs an SQL delete query on all rows in the QuerySet and returns the number of objects deleted and a dictionary with the number of deletions per object type.


```python
In [1]: UserProfile.objects.filter(email__icontains='ojete').delete()
Out [1]: {10, 'UserProfile': 10}
```

### `filter()`
Returns a new QuerySet containing objects that match the given lookup parameters.

The lookup parameters (**kwargs) should be in the format described in [Field lookups](#field-lookups) below. Multiple parameters are joined via AND in the underlying SQL statement.

### `exclude()`
Returns a new QuerySet containing objects that do not match the given lookup parameters.

The lookup parameters (**kwargs) should be in the format described in [Field lookups](#field-lookups) below. Multiple parameters are joined via AND in the underlying SQL statement, and the whole thing is enclosed in a NOT().

### `order_by()`
Orders the items in the queryset by the parameters passed, you can add the "-" symbol to any parameter to reverse the order
```python
Entry.objects.all().order_by('-pub_date', 'headline')
```
The result above will be ordered by pub_date descending, then by headline ascending. The negative sign in front of "-pub_date" indicates descending order. Ascending order is implied. To order randomly, use "?", like so:
```python
Entry.objects.order_by('?')
```
>:warning: The example above may be expensive and slow.

### `distinct()`
Returns a new QuerySet that uses SELECT DISTINCT in its SQL query. This eliminates duplicate rows from the query results for the given parameter.
>:warning: To work correctly the `distinct` method must be called from a queryset ordered by the desired field.
Examples:
```python
Entry.objects.order_by('pub_date').distinct('pub_date')

Entry.objects.order_by('blog').distinct('blog')

Entry.objects.order_by('author', 'pub_date').distinct('author', 'pub_date')

Entry.objects.order_by('blog__name', 'mod_date').distinct('blog__name', 'mod_date')

Entry.objects.order_by('author', 'pub_date').distinct('author')
```

### `values_list()`
Returns a queryset that returns tuples when iterated over. Each tuple contains the value from the respective field passed into the values_list() call — so the first item is the first field, etc. For example:
```python
>>> Entry.objects.values_list('id', 'headline')
[(1, 'First entry'), ...]
```
If you only pass in a single field, you can also pass in the flat parameter. If True, this will mean the returned results are single values, rather than one-tuples. An example should make the difference clearer:
```python
>>> Entry.objects.values_list('id').order_by('id')
[(1,), (2,), (3,), ...]


>>> Entry.objects.values_list('id', flat=True).order_by('id')
[1, 2, 3, ...]
```
It is an error to pass in flat when there is more than one field.

If you don’t pass any values to values_list(), it will return all the fields in the model, in the order they were declared.

A common need is to get a specific field value of a certain model instance. To achieve that, use values_list() followed by a get() call:
```python
>>> Entry.objects.values_list('headline', flat=True).get(pk=1)
'First entry'
```
`values_list()` is intended as optimization for a specific use case: retrieving a subset of data without the overhead of creating a model instance. This metaphor falls apart when dealing with many-to-many and other multivalued relations (such as the one-to-many relation of a reverse foreign key) because the “one row, one object” assumption doesn’t hold.

For example, notice the behavior when querying across a ManyToManyField:
```python
>>> Author.objects.values_list('name', 'entry__headline')
[('Noam Chomsky', 'Impressions of Gaza'),
 ('George Orwell', 'Why Socialists Do Not Believe in Fun'),
 ('George Orwell', 'In Defence of English Cooking'),
 ('Don Quixote', None)]
 ```
Authors with multiple entries appear multiple times and authors without any entries have None for the entry headline.

Similarly, when querying a reverse foreign key, None appears for entries not having any author:
```python
>>> Entry.objects.values_list('authors')
[('Noam Chomsky',), ('George Orwell',), (None,)]
```

### `first()`
Returns the first object matched by the queryset, or None if there is no matching object. If the QuerySet has no ordering defined, then the queryset is automatically ordered by the primary key.

### `last()`
Works like first(), but returns the last object in the queryset.

### `latest()`
Returns the latest object in the table, by date, using the field_name provided as the date field.

This example returns the latest UserProfile in the table, according to the modified field:
```python
UserProfile.objects.latest('modified')
```
Like `get()`, `earliest()` and `latest()` raise `DoesNotExist` if there is no object with the given parameters.

>:warning: `earliest()` and `latest()` may return instances with null dates.
Since ordering is delegated to the database, results on fields that allow null values may be ordered differently if you use different databases. For example, PostgreSQL and MySQL sort null values as if they are higher than non-null values, while SQLite does the opposite.
You may want to filter out null values:
```python
UserProfile.objects.filter(modified__isnull=False).latest('modified')
```

### `earliest()`
Works otherwise like latest() except the direction is changed.

### `count()`
Returns an integer representing the number of objects in the database matching the QuerySet. The count() method never raises exceptions.

### `exists()`
Returns True if the `QuerySet` contains any results, and False if not. This tries to perform the query in the simplest and fastest way possible, but it does execute nearly the same query as a normal `QuerySet` query.

## Field lookups

Field lookups are how you specify the meat of an SQL `WHERE` clause. They’re specified as keyword arguments to the `QuerySet` methods `filter()`, `exclude()` and `get()`.

They can also be used as `GET` url parameters in the admin site

As a convenience when no lookup type is provided (like in `Entry.objects.get(id=14)`) the lookup type is assumed to be exact.

>:information_source: You can follow relations and access class attributes in lookups using a double underscore, also note thet when dealing with many-to-many and one-to-many realtions if **any** related model satisfies the condition the lookup will be true.
>Examples:
>
>In this example we follow the relation SentSubscription -> Subscription -> UserProfile and use the user's email as lookup field
```python
SentSubscription.objects.filter(subscription__user__email='francesc.lapedriza@immfly.com')
```
>This will return all resources that have **any** parent channel that has EasyJet as **one** of its airlines
```python
Resource.objects.filter(channels__airlines__icao_code='EZY')
```

### exact

Exact match. If the value provided for comparison is None, it will be interpreted as an SQL `NULL` (see `isnull` for more details).

Examples:
```python
Entry.objects.get(id__exact=14)
Entry.objects.get(id__exact=None)
```
SQL equivalents:
```sql
SELECT ... WHERE id = 14;
SELECT ... WHERE id IS NULL;
```

### iexact

Case-insensitive exact match. If the value provided for comparison is None, it will be interpreted as an SQL NULL (see isnull for more details).

### contains

Case-sensitive containment test.

Example:
```python
UserProfile.objects.get(email__contains='francesc.lapedriza')
```

### icontains

Case-insensitive containment test.

### in

In a given list.

Example:
```python
Entry.objects.filter(id__in=[1, 3, 4])
```

You can also use a queryset to dynamically evaluate the list of values instead of providing a list of literal values:
```python
inner_qs = Blog.objects.filter(name__contains='Django')
entries = Entry.objects.filter(blog__in=inner_qs)
```

### gt

Greater than.

Example:
```python
Entry.objects.filter(id__gt=4)
```

### gte

Greater than or equal to.

### lt

Less than.

### lte

Less than or equal to.

### startswith

Case-sensitive starts-with.

Example:
```python
Entry.objects.filter(headline__startswith='Will')
```

### istartswith

Case-insensitive starts-with.

### endswith

Case-sensitive ends-with

### iendswith

Case-insensitive ends-with.

### range

Range test (inclusive).

Example:
```python
import datetime
start_date = datetime.date(2017, 1, 1)
end_date = datetime.date(2017, 12, 31)
Entry.objects.filter(pub_date__range=(start_date, end_date))
```

>:warning: Filtering a DateTimeField with dates won’t include items on the last day, because the bounds are interpreted as “0am on the given date”. Generally speaking, you can’t mix dates and datetimes.

### isnull

Takes either True or False, which correspond to SQL queries of IS NULL and IS NOT NULL, respectively.

Example:
```python
Entry.objects.filter(pub_date__isnull=True)
```

### regex

Case-sensitive regular expression match.

The regular expression syntax is that of the database backend in use. In the case of SQLite, which has no built in regular expression support, this feature is provided by a (Python) user-defined REGEXP function, and the regular expression syntax is therefore that of Python’s re module.

Example:
```python
Entry.objects.get(title__regex=r'^(An?|The) +')
```

## Complex lookups with `Q` objects
Keyword argument queries – in `filter()`, etc. – are “AND”ed together. If you need to execute more complex queries (for example, queries with OR statements), you can use Q objects.

A Q object (django.db.models.Q) is an object used to encapsulate a collection of keyword arguments. These keyword arguments are specified as in “Field lookups” above.

For example, this Q object encapsulates a single LIKE query:
```python
from django.db.models import Q
Q(question__startswith='What')
```
Q objects can be combined using the `&` and `|` operators. When an operator is used on two Q objects, it yields a new Q object.

For example, this statement yields a single Q object that represents the “OR” of two "question__startswith" queries:
```python
Q(question__startswith='Who') | Q(question__startswith='What')
```
This is equivalent to the following SQL WHERE clause:
```sql
WHERE question LIKE 'Who%' OR question LIKE 'What%'
```
You can compose statements of arbitrary complexity by combining Q objects with the `&` and `|` operators and use parenthetical grouping. Also, Q objects can be negated using the `~` operator, allowing for combined lookups that combine both a normal query and a negated (NOT) query:
```python
Q(question__startswith='Who') | ~Q(pub_date__year=2005)
```
Each lookup function that takes keyword-arguments (e.g. `filter()`, `exclude()`, `get()`) can also be passed one or more Q objects as arguments. If you provide multiple Q object arguments to a lookup function, the arguments will be “AND”ed together. For example:
```python
Poll.objects.get(
    Q(question__startswith='Who'),
    Q(pub_date=date(2005, 5, 2)) | Q(pub_date=date(2005, 5, 6))
)
```
… roughly translates into the SQL:
```sql
SELECT * from polls WHERE question LIKE 'Who%'
    AND (pub_date = '2005-05-02' OR pub_date = '2005-05-06')
```
Lookup functions can mix the use of Q objects and keyword arguments. All arguments provided to a lookup function (be they keyword arguments or Q objects) are “AND”ed together. However, if a Q object is provided, it must precede the definition of any keyword arguments. For example:
```python
Poll.objects.get(
    Q(pub_date=date(2005, 5, 2)) | Q(pub_date=date(2005, 5, 6)),
    question__startswith='Who',
)
 ```
… would be a valid query, equivalent to the previous example; but:
```python
Poll.objects.get(
    question__startswith='Who',
    Q(pub_date=date(2005, 5, 2)) | Q(pub_date=date(2005, 5, 6))
)
```
… would not be valid.

# Aggregation

With the `aggregate()` method you can obtain aggregation data from a queryset.

`aggregate()` returns a dictionary of aggregate values (averages, sums, etc.) calculated over the QuerySet. Each argument to aggregate() specifies a value that will be included in the dictionary that is returned.

The aggregation functions that are provided by Django are described in [Aggregation Functions](#aggregation-functions) below. Since aggregates are also query expressions, you may combine aggregates with other aggregates or values to create complex aggregates.

For example, when you are working with blogs, you may want to know the number of entries on each blog:
```python
from django.db.models import Count
q = Blog.objects.aggregate(Count('entry'))
{'entry__count': 16}
```

## Aggregation functions

>:warning: Aggregation functions return None when used with an empty QuerySet. For example, the Sum aggregation function returns None instead of 0 if the QuerySet contains no entries. An exception is Count, which does return 0 if the QuerySet is empty.

### Count
Returns the number of objects that are related through the provided expression.

Has one optional argument:

`distinct`

If distinct=True, the count will only include unique instances. This is the SQL equivalent of `COUNT(DISTINCT <field>)`. The default value is False.



### Max
Returns the maximum value of the given expression.

### Min
Returns the minimum value of the given expression.

### StdDev
Returns the standard deviation of the data in the provided expression.

Has one optional argument:

`sample`

By default, StdDev returns the population standard deviation. However, if sample=True, the return value will be the sample standard deviation.

### Sum
Computes the sum of all values of the given expression.


### Variance
Returns the variance of the data in the provided expression.

Has one optional argument:

`sample`

By default, Variance returns the population variance. However, if sample=True, the return value will be the sample variance.
